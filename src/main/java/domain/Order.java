package domain;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Collections;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;

public class Order
{
    private int orderNr;
    private boolean isStudentOrder;
    private boolean isWeekend;
    private ArrayList<MovieTicket> tickets;

    public Order(int orderNr, boolean isStudentOrder)
    {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;

        tickets = new ArrayList<MovieTicket>();
    }

    public int getOrderNr()
    {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket)
    {
        tickets.add(ticket);
    }

    public double calculatePrice()
    {
        //Arraylist for storing ticket prices
        ArrayList<Double> ticketPrices = new ArrayList<>();

        //Get base ticket price
        double ticketBasePrice= tickets.get(0).getPrice();

        //Add appropriate premium, add each ticket price to prices list
        double ticketPrice;
        for (MovieTicket movieTicket: tickets) {
            ticketPrice=ticketBasePrice;

            if(movieTicket.isPremiumTicket()) {
                if (isStudentOrder){
                    ticketPrice += 2;
                }
                else ticketPrice += 3;
            }
            ticketPrices.add(ticketPrice);
        }

        //Functionality for getting day of the week for the screening.
        DayOfWeek screeningDay = tickets.get(0).getScreeningDate().getDayOfWeek();
        if (screeningDay==DayOfWeek.FRIDAY || screeningDay==DayOfWeek.SATURDAY || screeningDay==DayOfWeek.SUNDAY ) isWeekend=true;

        if(isStudentOrder || !isWeekend) {
            //Sort prices list by price(low to high) & remove cheapest 2nd free tickets
            Collections.sort(ticketPrices);
            int secondFree = ticketPrices.size() / 2;
            ticketPrices.subList(0, secondFree).clear();
        }

        //Calculate total price
        double total=0;
        for (Double d : ticketPrices) {
            total+=d;
        }

        //10% group discount for non-students during the weekends
        //IMPORTANT: Ik ga er van uit dat de groepskorting niet samengaat met het 2e kaartje gratis.
        //Hierom worden studenten niet meegenomen in de korting, en niet-studenten alleen in het weekend.
        //Dit is hoe ik de opdracht heb begrepen. Verander de onderstaande functie als dit niet het geval is.
        if (!isStudentOrder && isWeekend && tickets.size()>5){
            total*=0.9;
        }

        return total;
    }

    public void export(TicketExportFormat exportFormat) {
        // Bases on the string representations of the tickets (toString), write
        // the ticket to a file with naming convention Order_<orderNr>.txt of
        // Order_<orderNr>.json

        if (exportFormat == TicketExportFormat. JSON) {
            JSONObject json = new JSONObject();
            for (int i = 0; i <= this.tickets.size(); i++) {
                json.put("Ticket " + i, this.tickets.indexOf(i));
            }
            if (!json.isEmpty()) {
                try {
                    FileWriter file = new FileWriter("src/main/resources/tickets/Order_" + this.orderNr + ".json");
                    file.write(json.toJSONString());
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (exportFormat == TicketExportFormat.PLAINTEXT) {
            try {
                FileWriter file = new FileWriter("src/main/resources/tickets/Order_" + this.orderNr + ".txt");
                for (int i = 0; i <= this.tickets.size(); i++) {
                    file.write(this.tickets.indexOf(i));
                }
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
